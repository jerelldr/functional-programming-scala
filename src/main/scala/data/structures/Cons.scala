package data.structures

/**
  * Created by Jerell on 9/5/2017.
  */
sealed trait List[+A]

case object Nil extends List[Nothing]

case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil         => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def sumStrings(strings: List[String]): String = strings match {
    case Nil         => ""
    case Cons(x, xs) => x + sumStrings(xs)
  }

  def product(ds: List[Double]): Double = ds match {
    case Nil         => 1.0
    case Cons(x, xs) => x * product(xs)
  }

  def tail[A](list: List[A]): List[A] = list match {
    case Nil        => Nil
    case Cons(_, t) => t
  }

  def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
    case Nil        => a2
    case Cons(h, t) => Cons(h, append(t, a2))
  }

  def apply[A](as: A*): List[A] = {
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))
  }

  def setHead[A](list: List[A], h: A): List[A] = list match {
    case Nil        => sys.error("tail of empty list")
    case Cons(_, t) => Cons(h, t)
  }

  def drop[A](l: List[A], n: Int): List[A] = {
    if (n <= 0) l
    else
      l match {
        case Nil        => Nil
        case Cons(_, t) => drop(t, n - 1)
      }
  }

  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Cons(h, t) if f(h) => dropWhile(t, f)
    case _                  => l
  }

  def main(args: Array[String]): Unit = {
    val list = List("a", "b")
    val newList = Cons("a", Cons("b", Cons("c", Nil)))
    println((list.toString))
    println((newList.toString))
    println(List.sumStrings(newList))

    val x = List(1, 2, 3, 4, 5) match {
      case Cons(x, Cons(4, _))                   => x
      case Nil                                   => 42
      case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
      case Cons(h, t)                            => h + sum(t)
      case _                                     => 101
    }
    println(x)

    println(List.tail(List()))
    println(List.setHead(List(1, 2, 3), 4))
    println(List.drop(List(1, 2, 3), 2))
    println(dropWhile[Int](List(1, 2, 3), x => x > 0))
  }
}
