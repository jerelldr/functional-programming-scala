package getting.started

/**
  * Created by Jerell on 8/6/2017.
  */
object sort {

  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    @annotation.tailrec
    def go(i: Int): Boolean = {
      if (i == (as.length - 2))
        ordered(as(as.length - 2), as(as.length - 1))
      else if (ordered(as(i), as(i + 1))) go(i + 1)
      else false
    }
    go(0)
  }

  def main(args: Array[String]): Unit = {
    println(isSorted(Array(7, 2, 1), (x: Int, y: Int) => x > y))
  }
}
