package getting.started

/**
  * Created by Jerell on 8/7/2017.
  */
object partial {

  def partial1[A, B, C](a: A, f: (A, B) => C): B => C = { (b: B) =>
    f(a, b)
  }

  def curry[A, B, C](f: (A, B) => C): A => (B => C) = { a => b =>
    f(a, b)
  }
}
