package getting.started

/**
  * Created by Jerell on 8/6/2017.
  */
object fibonacci {
  def abs(n: Int): Int =
    if (n < 0) -n
    else n

  /**
    * my answer
    *
    * @param x
    * @return
    */
  def myFib(x: Int): Int = {
    if (x == 0 || x == 1)
      return x
    return myFib(x - 1) + myFib(x - 2)
  }

  /**
    * correct answer
    *
    * @param n
    * @return
    */
  def fib(n: Int): Int = {
    @annotation.tailrec
    def loop(n: Int, acc: Int, cur: Int): Int =
      if (n == 0) acc
      else loop(n - 1, cur, acc + cur)
    loop(n, 0, 1)
  }

  def main(args: Array[String]): Unit = {
    val input = List(0, 1, 2, 3, 4, 5)

    println("My Answer")
    input.map(n => println(myFib(n)))

    println("Correct Answer")
    input.map(n => println(fib(n)))
  }
}
